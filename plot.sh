#!/bin/sh

WAV=$1
OPATH=$2
if [ -z "$OPATH" ]; then
    OPATH=./
fi;

if [ -z "$WAV" ] || [ -z "$OPATH" ]; then
    echo -e "Usage: plot.sh file.wav [/path/to/save]"
else
    if [ -f "$WAV" ] && [ -d "$OPATH" ]; then
        rm -f data.*
        octave -q -W audio2neuro.m $WAV $OPATH
        if [ -f "$OPATH/data.js" ] && [ -f "$OPATH/data.txt" ]; then
            SUBSTR="src=\".*\.wav\""
            sed -i "s|$SUBSTR|src=\"$WAV\"|g" plotter.html
        fi;
    else
        if [ ! -f "$WAV" ]; then
            echo -e "Error: file $WAV is not existed"
        fi;
        if [ ! -d "$OPATH" ]; then
            echo -e "Error: folder $OPATH is not existed"
        fi;
    fi;
fi;
