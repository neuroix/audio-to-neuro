# Transposing audio signal to neuro waves

This software is created for research and learning purposes. Its goal to transpose audio signal to neuro waves (EEG) via fast Fourier transform algorithm.

## License information

This software is free and opensource software, so you can redistribute it and/or modify it under the terms of the [The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0).

## Developer

Please contact via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).
