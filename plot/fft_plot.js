var iters      = [];
var data_delta = [];
var data_theta = [];
var data_alpha = [];
var data_beta  = [];
var data_gamma = [];
var data_eeg_6 = [];
var data_eeg_7 = [];
var data_eeg_8 = [];

if ( typeof fftjson === 'undefined' || fftjson == null )  {
	throw new Error("JSON data is not available!");
}

for(var i=0; i<fftjson["eeg"].length; i++) {
	data_delta.push( fftjson["eeg"][i][0] );
	data_theta.push( fftjson["eeg"][i][1] );
	data_alpha.push( fftjson["eeg"][i][2] );
	data_beta.push ( fftjson["eeg"][i][3] );
	data_gamma.push( fftjson["eeg"][i][4] );
	data_eeg_6.push( fftjson["eeg"][i][5] );
	data_eeg_7.push( fftjson["eeg"][i][6] );
	data_eeg_8.push( fftjson["eeg"][i][7] );
	iters.push(i);
}

var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
	type: 'line',
	data: {
		labels: iters,
		datasets: [
		{
			label: 'δ-wave',
			data: data_delta,
			fill: false,
			backgroundColor: 'rgba(235, 82, 87, 0.9)',
			borderColor: 'rgba(235, 82, 87, 0.7)',
		},
		{
			label: 'θ-wave',
			data: data_theta,
			fill: false,
			backgroundColor: 'rgba(54, 162, 235, 0.9)',
			borderColor: 'rgba(54, 162, 235, 0.7)',
		},
		{
			label: 'α-wave',
			data: data_alpha,
			fill: false,
			backgroundColor: 'rgba(4, 207, 14, 0.9)',
			borderColor: 'rgba(4, 207, 14, 0.7)',
		},
		{
			label: 'β-wave',
			data: data_beta,
			fill: false,
			backgroundColor: 'rgba(217, 22, 191, 0.9)',
			borderColor: 'rgba(217, 22, 191, 0.7)',
		},
		{
			label: 'γ-wave',
			data: data_gamma,
			fill: false,
			backgroundColor: 'rgba(245, 220, 0, 0.9)',
			borderColor: 'rgba(245, 220, 0, 0.7)',
		},
		{
			label: 'wave #5',
			data: data_eeg_6,
			fill: false,
			backgroundColor: 'rgba(57, 250, 192, 0.9)',
			borderColor: 'rgba(57, 250, 192, 0.7)',
			hidden: true,
		},
		{
			label: 'wave #6',
			data: data_eeg_7,
			fill: false,
			backgroundColor: 'rgba(242, 174, 170, 0.9)',
			borderColor: 'rgba(242, 174, 170, 0.7)',
			hidden: true,
		},
		{
			label: 'wave #7',
			data: data_eeg_8,
			fill: false,
			backgroundColor: 'rgba(171, 171, 245, 0.9)',
			borderColor: 'rgba(171, 171, 245, 0.7)',
			hidden: true,
		},
		],
	},
	options: {
		scales: {
			yAxes: [{
				ticks: {
					display: false
				},
				display: false
			}],
			xAxes: [{
				gridLines: {
					display: false,
					drawTicks: false,
					color: 'rgba(0, 17, 46, 0.5)'
				},
			}],
		}
	}
});
