%//JSONLab bindings
%//Download and unzip JSONLab from https://github.com/fangq/jsonlab

fprintf(
    "USAGE:\nfilename=\"%s\"; saveto=\"%s\"; audio2neuro;\n%s\n",
    "./wav/lana-del-rey_ss.wav",
    "./",
    "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
);

arg_list = argv();
if length(arg_list) == 2
    filename = arg_list{1};
    saveto   = arg_list{2};
end

addpath('~/git/github/jsonlab');

%//Output spectrogram

spath     = './spectrogram.jpg';
spathbw   = './spectrogram-snd.jpg';
[x, Fs]   = audioread(filename);
win       = fix(50*Fs/1000);      %// 1640 ms data window
fftn      = 2^nextpow2(win);      %// next highest power of 2
[S, f, t] = specgram(
    x(:,1),                       %// select single channel
    fftn,
    Fs,
    win,
    fix(5*Fs/1000)                %// one spectral slice every 164 ms
);
S = abs(S(2:fftn*16000/Fs,:));    %// magnitude in range 0<f<=16000 Hz.
S = S/max(S(:));                  %// normalize magnitude so that max is 0 dB.
S = max(S, 10^(-40/10));          %// clip below -40 dB.
S = min(S, 10^(-3/10));           %// clip above -3 dB.
%//image = imagesc(t, f, log(S));    %// display in log scale
%//set(gca, "ydir", "normal");       %// set the normal 'y' direction
%//set(gca, "position", [0,0,1,1]);  %// set no white margins in output img
%//axis("off","nolabel");            %// remove axises and remove labels
%//saveas(gca, spath);
imwrite( ind2rgb(uint8(flipud(S)*3^9),jet(256)) ,spath);
imwrite( ind2rgb(uint8(flipud(S)*2.6^9),viridis(256)) ,spathbw);

%//FFT setup
%//FFTSize set to 65536, but https://developer.mozilla.org/en-US/docs/Web/API/AnalyserNode/fftSize

wintype = 0; %// 0 - hamming, 1 - gauss, 2 - hann, 3 - blackman
debug   = 0;
eegsave = 1;
grafix  = 0;
sss     = 15;
fftsize = 2^sss; %// 32768
samples = [1, fftsize ];

fprintf("stats: frequency resolution Fs/fftsize=(%d/%d)=%d Hz\n", Fs, fftsize, Fs/fftsize);

%//Load File
file    = filename;
info    = audioinfo( file );
iters   = floor(info.TotalSamples / fftsize) + 1;
eeg     = zeros(iters, 8);
si      = 0;

while si < iters
  sis = (si*fftsize+1);
  sie = ((si+1)*fftsize);
  if (sie > info.TotalSamples)
    sie = info.TotalSamples;
  end
  if grafix == 0
    fprintf("iter %03d/%03d: read samples [%09d,%09d] from %s\n", (si+1), iters, sis, sie, file);
  end

  [y,Fs] = audioread( file, [ sis sie ]);

  %//Mixing channels
  [m, n] = size(y);
  if n == 2
    ymix    = y(:, 1) + y(:, 2);
    peakAmp = max(abs(ymix));
    if peakAmp > 0
      ymix  = ymix/peakAmp;
    end
    peakL   = max(abs(y(:, 1)));
    peakR   = max(abs(y(:, 2)));
    maxPeak = max([peakL peakR]);
    ymix = ymix*maxPeak;
  else
    ymix = y;
  end

  if wintype == 0
    win   = hamming(fftsize);
  elseif wintype == 1
    win   = gausswin(fftsize);
  elseif wintype == 2
    win   = hann(fftsize);
  else
    win   = blackman(fftsize);
  end
  y_fft   = abs( fft(ymix, fftsize) / sum(win) );
  y_fft   = mean(y_fft,2);
  y_fft_h = y_fft(1:fftsize/2+1);
  y_fft_h(2:end-1) = 2*y_fft_h(2:end-1);
  %//y_fft = mean(y_fft,2);
  %//y_fft = fftshift(y_fft);

  telms = 0;
  mtx   = zeros(8, 2);
  i     = 0;
  while i <= log2(length(y_fft_h)-1)
    s        = floor((2^(i-1)));
    e        = 2^(i);
    sv       = 20*log10(y_fft_h(s+1:e));
    r        = rem((i), 8) + 1;
    c        = floor(i/8) + 1;
    mtx(r,c) = sum(sv)/length(sv);
    telms    = telms + length(sv);
    if ( debug )
      fprintf("\t(%d,%d): %06d..%06d, subvector len=%05d sum=%08d\n", r, c, s, e, length(sv), mtx(r,c));
    end
    i = i + 1;
  end

  if ( debug )
    fprintf("\ttotal elems %d (fftsize: 2^%d=%d)\n",telms,sss,fftsize);
  end

  si = si + 1;
  eeg(si,1:8) = sum(mtx,2)/2;
end

%//Save eeg matrix to CSV & JSON
if ( eegsave )
  csvwrite([ saveto 'data.txt' ], eeg);
  savejson('eeg', eeg, 'FileName', [ saveto 'data.js' ], 'JSONP', 'ffteeg');
end

%//Plot eeg matrix
if ( grafix )
  figure;
  plot(eeg(1:iters,1:8));
  ylim([-100 -40])
  xlabel('iters');
  ylabel('power');
  title('eeg');
end
