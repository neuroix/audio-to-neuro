#!/bin/bash

ARG=$1

if [ ! -z "$ARG" ]; then
    if [ -d "$ARG" ]; then
        HOME=$ARG
    fi;
fi;

FFTREPO=$HOME/git/neuroix/audio-to-neuro
PHEIXSAVE=$HOME/git/pheix-pool/core/www/media/upload
PHEIXPATH=$HOME/git/pheix-pool/core/www
PHEIXDB=$PHEIXPATH/conf/system/neuroix_eeg.tnk
PHEIXLIB=$PHEIXPATH/admin/libs/modules
PHEIXEXT=$PHEIXPATH/admin/libs/extlibs

readarray -t FILES < <(perl -I$PHEIXLIB -I$PHEIXEXT -MNeuroix::EEG -le "@a=Neuroix::EEG->new(\"$PHEIXPATH/\")->getUnprocessedData(\"*\"); foreach(@a) {print \"$PHEIXPATH/\".\$_->{fpath}}")

for FILE in "${FILES[@]}"
do
    BASEFILE=$(basename -- $FILE)
    RECID=${BASEFILE%.*}
    TMPFLDR=/tmp/pheix/$RECID
    if [ ! -f "$TMPFLDR/.lock" ]; then
        if [ -d "$TMPFLDR" ]; then
            rm -rf $TMPFLDR
        fi;
        mkdir -p $TMPFLDR
        echo -e "start:\t\t"`date` > $TMPFLDR/.lock
        ffmpeg -loglevel panic -y -i $FILE -acodec pcm_s16le -ar 32768 $TMPFLDR/$RECID.wav
        if [ $? -ne 0 ]; then
            echo -e "ffmpeg err:\t"`date` >> $TMPFLDR/.lock
        fi;

        octave -W -q $FFTREPO/audio2neuro.m $TMPFLDR/$RECID.wav $TMPFLDR/$RECID\_
        JSFILE=$TMPFLDR/$RECID\_data.js
        CSVFILE=$TMPFLDR/$RECID\_data.txt
        if [ -f "$JSFILE" ] && [ -f "$CSVFILE" ]; then
            cp $JSFILE $PHEIXSAVE
            cp $CSVFILE $PHEIXSAVE
            perl -I$PHEIXLIB -I$PHEIXEXT -MNeuroix::EEG -e "Neuroix::EEG->new(\"$PHEIXPATH/\")->setDataProcessed($RECID,\"$PHEIXSAVE/$RECID\_data.js\", \"$PHEIXSAVE/$RECID\_data.txt\")"
        else
            echo -e "octave err:\t"`date` >> $TMPFLDR/.lock
        fi;
        echo -e "end:\t\t"`date` >> $TMPFLDR/.lock
        mv $TMPFLDR/.lock $TMPFLDR/.info
    fi;
done
